import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})

export class DataService {
  urlApi;

  constructor(private http:HttpClient) { 
    this.urlApi = 'http://movie0706.cybersoft.edu.vn/api'
  }

  getDanhSachPhim (){
    return ['phimA','phimB','phimC'];
  }

  get(uri: string): Observable<any>{
    return this.http.get(this.urlApi + '/' + uri).pipe(
      tap((data: any)=>{
        // Loading
      }),
      catchError(err =>{
        return this.handleErr(err);
      })
    )
  }

  dangKy(user): Observable<any>{
    const url = this.urlApi + "/QuanLyNguoiDung/DangKy"
    return this.http.post(url,user).pipe(
      tap((data: any)=>{
        // Loading
      }),
      catchError(err =>{
        return this.handleErr(err);
      })
    )
  }

  handleErr(err) {
    switch (err.status) {
      case 500:
        alert(err.error)
        break;
      
        default:
          break;
    }
    return throwError(err);
  }
}
