import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {Routes, RouterModule} from '@angular/router'
// import { TrangChuComponent } from './home/trang-chu/trang-chu.component';
// import { ChiTietPhimComponent } from './home/chi-tiet-phim/chi-tiet-phim.component';
// import { DatVeComponent } from './home/dat-ve/dat-ve.component';
// import { DangKyComponent } from './home/dang-ky/dang-ky.component';
// import { DangNhapComponent } from './home/dang-nhap/dang-nhap.component';
import { HomeModule } from './home/home.module';
import { HttpClientModule } from '@angular/common/http';

const appRoute: Routes = [
  // {path:'',component:TrangChuComponent},
  // {path:'chitietphim',component:ChiTietPhimComponent},
  // {path:'datve',component:DatVeComponent},
  // {path:'dangky',component:DangKyComponent},
  // {path:'dangnhap',component:DangNhapComponent},
  {path:'home', loadChildren: ()=>HomeModule},

  {path:'', loadChildren: ()=>HomeModule},

]
// chỉ app module mới dùng forRoot, còn lại dùng forChild
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,RouterModule.forRoot(appRoute),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
