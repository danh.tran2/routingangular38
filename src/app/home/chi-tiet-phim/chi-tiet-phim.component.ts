import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/_core/services/data.service';

@Component({
  selector: 'app-chi-tiet-phim',
  templateUrl: './chi-tiet-phim.component.html',
  styleUrls: ['./chi-tiet-phim.component.scss'],
})

export class ChiTietPhimComponent implements OnInit {
  maPhim: any;
  tenPhim: any;
  movie: any;

  constructor(
    private activedRoute: ActivatedRoute,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.getParamsFromURL();
    this.layChiTietPhim();
  }

  getParamsFromURL() {
    this.maPhim = this.activedRoute.snapshot.paramMap.get('id');
    this.activedRoute.queryParams.subscribe((params: any) => {
      this.tenPhim = params.tenPhim;
    });
  }

  layChiTietPhim() {
    const uri = `QuanLyPhim/LayThongTinPhim?MaPhim=${this.maPhim}`;
    this.dataService.get(uri).subscribe((data: any) => {
      this.movie = data;
      console.log(data);
    });
  }
}
