import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/_core/services/data.service';

@Component({
  selector: 'app-dang-ky',
  templateUrl: './dang-ky.component.html',
  styleUrls: ['./dang-ky.component.scss'],
})
export class DangKyComponent implements OnInit {
  danhSachNguoiDung = [];
  constructor(
    private dataService: DataService
  ) {}

  ngOnInit(): void {}

  DangKy(formValue) {
    // const user = {
    //   TaiKhoan: formValue.TaiKhoan,
    //   MatKhau: formValue.MatKhau,
    //   Email: formValue.Email,
    //   SoDienThoai: formValue.SoDienThoai,
    //   HoTen: formValue.HoTen,
    // }
    // this.danhSachNguoiDung.push(user)

    const user = {
      taiKhoan: formValue.TaiKhoan,
      matKhau: formValue.MatKhau,
      email: formValue.Email,
      soDt: formValue.SoDienThoai,
      maNhom: 'GP01',
      maLoaiNguoiDung: 'QuanTri',
      hoTen: formValue.HoTen,
    };

    this.dataService.dangKy(user).subscribe(
      (data) => {
        console.log(user)
        alert('Đăng ký thành công !');
      },
      (err) => {}
    );
  }
}
