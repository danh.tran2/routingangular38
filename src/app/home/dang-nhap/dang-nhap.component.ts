import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-dang-nhap',
  templateUrl: './dang-nhap.component.html',
  styleUrls: ['./dang-nhap.component.scss'],
})
export class DangNhapComponent implements OnInit {
  public formDangNhap: FormGroup;

  constructor() {
    this.formDangNhap = new FormGroup({
      TaiKhoan: new FormControl(null, Validators.required),
      MatKhau: new FormControl(null, Validators.minLength(4)),
    });
  }

  ngOnInit(): void {}

  DangNhap(): void {
    console.log(this.formDangNhap)
  }
}
