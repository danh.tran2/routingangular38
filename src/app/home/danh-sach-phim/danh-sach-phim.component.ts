import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/_core/services/data.service';

@Component({
  selector: 'app-danh-sach-phim',
  templateUrl: './danh-sach-phim.component.html',
  styleUrls: ['./danh-sach-phim.component.scss']
})
export class DanhSachPhimComponent implements OnInit {

  constructor(private dataService: DataService) { }

  danhSachPhim: any =[]

  ngOnInit(): void {
    // console.log(this.danhSachPhim)
    this.layDanhSachPhim()
  }

  layDanhSachPhim(){
    const uri = 'QuanLyPhim/LayDanhSachPhim?maNhom=GP01';
    this.dataService.get(uri).subscribe(data=>{
      this.danhSachPhim = data;
      console.log(this.danhSachPhim)
    })
  }

}
