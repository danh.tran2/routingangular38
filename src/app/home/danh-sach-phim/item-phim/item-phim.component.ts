import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item-phim',
  templateUrl: './item-phim.component.html',
  styleUrls: ['./item-phim.component.scss']
})
export class ItemPhimComponent implements OnInit {
  @Input() phim;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  chiTiet(){
    this.router.navigate(['/chi-tiet/', this.phim.maPhim],{
      queryParams: {tenPhim: this.phim.tenPhim}
    });
  }

}
