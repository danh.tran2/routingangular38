import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrangChuComponent } from './trang-chu/trang-chu.component';
import { ChiTietPhimComponent } from './chi-tiet-phim/chi-tiet-phim.component';
import { DatVeComponent } from './dat-ve/dat-ve.component';
import { DangKyComponent } from './dang-ky/dang-ky.component';
import { DangNhapComponent } from './dang-nhap/dang-nhap.component';
import { HomeTemplateComponent } from './home-template/home-template.component';
import { Routes, RouterModule } from '@angular/router';
import { DanhSachPhimComponent } from './danh-sach-phim/danh-sach-phim.component';
import { ItemPhimComponent } from './danh-sach-phim/item-phim/item-phim.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

// khi /home thì sẽ load mặc định là path '', nhưng module ko có giao diện nên phải set component load lên
const homeRoute: Routes = [
  {
    path: '',
    component: HomeTemplateComponent,
    children: [
      //  - /home
      { path: '', component: TrangChuComponent },
      //  - /home/trangchu
      { path: 'trangchu', component: TrangChuComponent },
      //  - /home/dangky
      { path: 'dangky', component: DangKyComponent },
      //  - /home/dangnhap
      { path: 'dangnhap', component: DangNhapComponent },
      //  - /home/danhsachphim
      { path: 'danhsachphim', component: DanhSachPhimComponent },
      { path: 'chi-tiet/:id', component: ChiTietPhimComponent },

    ],
  },
];

@NgModule({
  declarations: [
    TrangChuComponent,
    ChiTietPhimComponent,
    DatVeComponent,
    DangKyComponent,
    DangNhapComponent,
    HomeTemplateComponent,
    DanhSachPhimComponent,
    ItemPhimComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(homeRoute), FormsModule, ReactiveFormsModule],
})
export class HomeModule {}
